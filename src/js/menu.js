console.log('%c START MENU !', 'color:green')

/********/
/* Menu */
/********/

const menu = {
  el: {
    nav: document.getElementsByClassName('navigation')[0],
    burger: document.getElementsByClassName('menu-burger')[0],
    header: document.getElementsByClassName('header')[0],
    icon: document.getElementsByClassName('open-close')[0]
  },
  init () {
    this.el.burger.addEventListener('click', (event) => {
      event.preventDefault()
      this.openCloseMenu()
    })
  },
  /**
   * Menu Ouvert
   */
  menuOpen () {
    /* ---------- */
    /* -- OPEN -- */
    /* ---------- */
    this.el.nav.classList.add('nav-open')
    this.el.icon.classList.add('icon-close')
    this.el.burger.classList.add('menu-open')
    this.el.icon.classList.remove('icon-menu')
    this.el.header.style.position = 'fixed'
    this.STATE = 'OPEN'
  },
  /**
   * Menu Fermé
   */
  menuClose () {
    /* ----------- */
    /* -- CLOSE -- */
    /* ----------- */
    this.el.nav.classList.remove('nav-open')
    this.el.icon.classList.remove('icon-close')
    this.el.burger.classList.remove('menu-open')
    this.el.icon.classList.add('icon-menu')
    this.el.header.style.position = 'absolute'
    this.STATE = 'CLOSE'
  },
  /**
   * Appel fonction Ouverture Fermeture Menu
   */
  openCloseMenu () {
    this.STATE === 'OPEN' ? this.menuClose() : this.menuOpen()
  },
  STATE: 'CLOSE'
}

// Event Listener DOMContentLoaded
window.addEventListener('DOMContentLoaded', menu.init())
