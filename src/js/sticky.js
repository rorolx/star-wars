console.log('%c START STICKY !', 'color:green')

/**********/
/* Sticky */
/**********/

const sticky = {
  // DOM elements
  el: {
    headerNav: document.getElementsByClassName('navigation')[0],
    contentHeader: document.getElementsByClassName('content-header')[0],
    contentAd: document.getElementsByClassName('content-ad')[0],
    stickyAd: document.getElementById('sticky-ad')
  },
  // espace custom (marge)
  gap: {
    gapTop: 30,
    gapBottom: 30
  },
  /*
   * init() sticky scroll
   */
  init () {
    this.initScroll()
  },
  initScroll () {
    // scroll addEventListener
    window.addEventListener('scroll', () => {
      this.scrollSticky(this.el.stickyAd)
    })
  },
  /*
  * position par defaut du sticky
  * style par défaut
  */
  defaultPosition (target) {
    target.style.position = 'absolute'
    target.style.top = 'initial'
    target.style.bottom = '0'
  },
  /*
  * position mis à jour
  * style dynamique
  */
  updatePosition (target) {
    target.style.position = 'fixed'
    target.style.top =
      this.el.headerNav.offsetHeight + this.gap.gapBottom + 'px'
    target.style.bottom = 'initial'
  },
  /**
   * getPosY return la position Y de l'élément
   */
  getPosY (element, posY) {
    if (element !== undefined) {
      const rect = element.getBoundingClientRect()
      return Math.floor(rect[posY])
    }
  },
  /**
   * getScrollY return la position Y de la page
   */
  getScrollY () {
    const scrollY = window.scrollY || window.pageYOffset || document.body.scrollTop
    this.scrollLastPos = scrollY
    return scrollY
  },
  /*
  * scroll dernière position avant scrollY
  */
  scrollLastPos: 0,
  /* scrollsticky
   * target = cible du sticky
   */
  scrollSticky (target) {
    if (this.scrollLastPos > this.getScrollY()) {
      // ---------------
      // scroll UP
      // ---------------

      const limitTop = this.getPosY(this.el.contentAd, 'top')

      if (this.getPosY(this.el.stickyAd, 'top') > limitTop) {
        if (this.getPosY(this.el.stickyAd, 'top') > this.el.headerNav.offsetHeight + this.gap.gapBottom) {
          this.updatePosition(target)
        }
      } else {
        target.style.position = 'static'
        target.style.top = limitTop
      }
    } else {
      // ---------------
      // scroll DOWN
      // ---------------

      const limitTop = this.getPosY(this.el.contentAd, 'top') - this.gap.gapTop
      const limitBottom = this.getPosY(this.el.contentAd, 'bottom')

      if (limitTop < this.getPosY(this.el.headerNav, 'bottom')) {
        if (limitBottom > this.getPosY(this.el.stickyAd, 'bottom') && limitBottom > this.el.stickyAd.offsetHeight) {
          this.updatePosition(target)
        } else {
          this.defaultPosition(target)
        }
      }
    }
  }
  //
}

// Event Listener DOMContentLoaded
window.addEventListener('DOMContentLoaded', sticky.init())
