console.log('%c START PARALLAX !', 'color:green')

/************/
/* Parallax */
/************/
const Rellax = require('rellax')

const rellax = new Rellax('.parallax', {
  center: true,
  horizontal: true
})
console.log(rellax)
