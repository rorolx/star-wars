const path = require("path");
const variables = require('./variables/variables.js');

module.exports = {
  plugins: {
    "postcss-nested": {},
    "postcss-pxtorem": {},
    "postcss-preset-env": {
      stage: 4,
      features: {
        "nesting-rules": true,
      },
      autoprefixer: {},
    },
    cssnano: {
      preset: [
        "default",
        {
          discardComments: {
            removeAll: true,
          },
        },
      ],
    },
    "postcss-import": {
      root: "./src/asstes/css/",
    },
    "postcss-simple-vars": {
      variables: variables
    },
  },
};
