# Page Star Wars

- Page responsive
- Pavé Ad Sticky
- Parallax

Répertoire distant ```public/index.html```

## Installation

Installation des packages.

```bash
npm install
```

## Déploiement environnement dev (logs)

```bash
npm run dev
```

## Déploiement environnement production

```bash
npm run build
```

## Parallax
Librairie [Rellax](https://www.npmjs.com/package/rellax)

*Ici définit sur la classe parallax*
```
const rellax = new Rellax('.parallax')
```
**Quick Start :**
Ajouter la class css ```parallax```

```html
<div class="parallax">
    I’m slow and smooth
</div> 
```

**Speed** *(Default Speed: -2)*
```html
<div
  class="parallax"
  data-rellax-speed="7"
  data-rellax-xs-speed="-5"
  data-rellax-mobile-speed="3"
  data-rellax-tablet-speed="-8"
  data-rellax-desktop-speed="1">
    I parallax at all different speeds depending on your screen width.
</div> 
```

**Horizontal Parallax**
```html
<div
  class="parallax"
  data-rellax-vertical-scroll-axis="x">
    Horizontal Parallax
</div>
```

Plus de configuration disponible sur la page [Rellax](https://www.npmjs.com/package/rellax)