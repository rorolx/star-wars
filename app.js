// IMPORTS

function importAllElements(e) {
  return e.keys().map(e);
}

// CSS
const css = importAllElements(require.context('./src/css/', false, /\.css$/));
// JS
const js = importAllElements(require.context('./src/js/', false, /\.js$/));
// FONTS
const fonts = importAllElements(require.context('./src/css/fonts/', false, /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/));
// IMAGES
const images = importAllElements(require.context('./src/images/', false, /\.(png|jpe?g|svg)$/));
