const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

// images Optimisation
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminOptipng = require('imagemin-optipng');

// variables.js file
const variables = require('./variables/variables.js');


const production = process.env.NODE_ENV === "production";

module.exports = {
 entry: { 
  bundle: ["./app.js"],
  },
  mode: "production",
  output: {
    path: path.resolve(__dirname, "public/"),
    filename: "assets/js/[name].js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                      targets: {
                      browsers: "> 0.25%",
                    },
                    bugfixes: true,
                    spec: true,
                  },
                ],
              ],
            },
          },
          "eslint-loader",
        ],
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,          
          {
            loader: "css-loader",
            options: {
              url:false,
              sourceMap: true,
            },
          },              
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
            },
          },  
        ],
      },
      {
        test: /.(jpe?g|png|gif|svg?)(\?[a-z0-9]+)?$/,
        exclude: /(fonts)/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'assets/images/'
          }
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(images)/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "./assets/css/fonts/",
            },
          },
        ],
      },
    ],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
          compress: {
            drop_console: production ? true : false,
          }
        },
        exclude: /(node_modules)/,
        test: /\.js$/,
        extractComments: false,
      })
    ]
  },
  plugins:
      [
        new CleanWebpackPlugin(),
        
        new MiniCssExtractPlugin({
          filename: "assets/css/[name].css",
          chunkFilename: "[id].css",
        }),
        new ImageminPlugin({
          plugins: [
            imageminMozjpeg({quality: 75}),
            imageminOptipng()
          ]
        }),
        new HtmlWebpackPlugin({
          template: "src/index.html",
          templateParameters: variables,
        }),
        new PreloadWebpackPlugin({
          rel: 'preload',
          include: 'allAssets',
          fileWhitelist: [/\.js/, /\.css/],
          as(entry) {
            if (/\.css$/.test(entry)) return 'style';
            if (/\.js$/.test(entry)) return 'script';
            return 'script';
          }
        }),
        new ScriptExtHtmlWebpackPlugin({
          defaultAttribute: 'async'  
        }),
      ],
};