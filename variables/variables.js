module.exports = {
  //main article container width
  main_width: "800px",

  // Colors
  color_social: "#9b9b9b",

  // Fonts
  font_default: "Arial",
  font_regular: "open_sansregular",
  font_bold: "open_sansbold",
  font_bold_italic: "open_sansbold_italic",

  // images
  bg_menu_mobile:'./images/header-bg.jpg',
  bg_menu_mobile_height: '400px',
  // large
  img_default_height: '500px',
  img_default_width: '512px', // 1024 / 2

  // small
  img_small_height: '250px',

 
  // block margin default
  margin: '30px',

  // Breakpoints 
  $XSS: "380px",
  $XS: "475px",
  $XM: "950px",
  $XL: "1200px",

  isDesktop() {
    if (window.innerWidth > this.$XM) {
      return true
    } else {
      return false
    }
  }
};
